package com.laotou99.starsmaze;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.laotou99.starsmaze.myThread.KeyThread;

import static com.laotou99.starsmaze.constant.Constant.threadWork;

public class Main5Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        FrameLayout fl1 = (FrameLayout)findViewById(R.id.frame1);

        Test1SurfaceView tsv1 = new Test1SurfaceView(this);
        threadWork=true;
        new KeyThread(tsv1).start();
        fl1.addView(tsv1);
    }
}
