package com.laotou99.starsmaze.constant;

/**
 * Created by wisdom on 2017-12-20.
 */

public class Constant {

    public static boolean threadWork =false;    //键盘监听线程工作标志位

    public static final float SPEED = 0.05f;    //移动速度
    public static final float[][] MOVE_XZ =     // 摄像机每个方向的移动位移
            {
                    {0,-SPEED},     //0-North  Z轴负向
                    {SPEED,0},      //1-EAST   X轴正向
                    {0,SPEED},      //2-SOUTH  Z轴正向
                    {-SPEED,0}      //3-WEST   X轴负向
            };

    public  static final float NEAR = 0.45f;       //可视区域最近端

    public static final float ICON_DIS=NEAR;     //图标离视点的距离

    public static final float ICON_WIDTH= 0.05f;    //图标尺寸
    public static final float ICON_HEGITH=0.1f;

    //迷宫地图
    public static final int[][] MAP=            //0 不可通过 1可通过
            {
                    {1,1,1,1,1,0,0,0,0,0},
                    {0,0,0,0,1,0,0,0,0,0},
                    {0,0,0,0,1,0,0,0,0,0},
                    {0,0,0,0,1,0,0,0,0,0},
                    {0,0,0,0,1,0,0,0,0,0},
                    {0,0,0,0,1,1,1,1,0,0},
                    {0,0,0,0,0,0,0,1,0,0},
                    {0,1,1,1,1,1,1,1,0,0},
                    {0,1,0,0,0,0,0,0,0,0},
                    {0,1,0,0,0,0,0,0,0,0}
            };


    public static final int[][] MAP_OBJECT=     //表示可遇晶体位置的矩阵

            {
                    {0,1,0,0,0,0,0,0,0,0},
                    {0,0,0,0,1,0,0,0,0,0},
                    {0,0,0,0,0,0,0,0,0,0},
                    {0,0,0,0,0,0,0,0,0,0},
                    {0,0,0,0,1,0,0,0,0,0},
                    {0,0,0,0,0,0,1,0,0,0},
                    {0,0,0,0,0,0,0,0,0,0},
                    {0,1,0,0,1,0,0,0,0,0},
                    {0,0,0,0,0,0,0,0,0,0},
                    {0,0,0,0,0,0,0,0,0,0}
            };
    public static final int OBJECT_COUNT=6;//可遇晶体的数量

    public static final float WALL_HEIGHT=2.8f;//墙的高度

    public static final float UNIT_SIZE=1.4f;//地面每个格子的大小

    public static final float FLOOR_Y=-1.0f;//地面的Y坐标

    //    public static final float CELL_Y=FLOOR_Y+WALL_HEIGHT;//屋顶的Y坐标
    public static final float CELL_Y=1.8f;//屋顶的Y坐标

    public static final int CAMERA_COL=1;//初始时Camera位置
    public static final int CAMERA_ROW=9;

    public static final float HALF_COLL_SIZE=UNIT_SIZE/2-0.2f;  //碰撞体尺寸

}
