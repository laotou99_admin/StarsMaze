package com.laotou99.starsmaze.myThread;

import com.laotou99.starsmaze.Test1SurfaceView;

import static com.laotou99.starsmaze.constant.Constant.*;
/**
 * Created by wisdom on 2017-12-20.
 */

public class KeyThread extends Thread
{

    Test1SurfaceView test1SurfaceView;

    public static int keyState=0;//0-没有键按下 1-上 2-下 3-左 4-右

    public KeyThread(Test1SurfaceView test1SurfaceView){
        this.test1SurfaceView = test1SurfaceView;
        System.out.println("KeyThread");
    }


    public void run(){
        System.out.println("KeyThread-=>run");
        System.out.println(threadWork);
        while(threadWork){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //System.out.println("1 keyState-=>"+keyState);
            if(keyState==0){
                continue;
            }
            System.out.println("2 keyState-=>"+keyState);

            try {
                if(keyState==1||keyState==2){
                    //上下键表示前进或后退
                    System.out.println("keyState-=>"+keyState);
                    int tempDirection = 0;
                    switch (keyState){
                        case 1://向上键代表前进
                            System.out.println("t qian");
                            tempDirection=test1SurfaceView.currentDirection;
                            System.out.println("tempDirection-=>"+tempDirection);

                            //前进时运动方向与当前方向相同
                            break;
                        case 2://向下键代表后退
                            System.out.println("t hou");
                            tempDirection=(test1SurfaceView.currentDirection+2)%4;
                            System.out.println("tempDirection-=>"+tempDirection);
                            //后退时运动方向与当前方向相反
                            break;
                    }

                    //计算运动后的XZ值
                    test1SurfaceView.heroX=test1SurfaceView.heroX+MOVE_XZ[tempDirection][0];
                    test1SurfaceView.heroZ=test1SurfaceView.heroZ+MOVE_XZ[tempDirection][1];
                    System.out.println("test1SurfaceView.heroX-=>"+test1SurfaceView.heroX);
                    System.out.println("test1SurfaceView.heroZ-=>"+test1SurfaceView.heroZ);
                    boolean backFlag=false;//恢复标记


                    //碰撞体左上侧点=======================begin
//                    float tempX = test1SurfaceView.heroX - HALF_COLL_SIZE;//左上侧点XY坐标

                    //碰撞体左上侧点=======================end



                }else{
                    switch (keyState){
                        case 3:
                            //向左代表左转身
                            test1SurfaceView.currentDirection=
                                    (test1SurfaceView.currentDirection-1+4)%4;
                            break;
                        case 4:
                            test1SurfaceView.currentDirection=
                                    (test1SurfaceView.currentDirection+1)%4;
                            break;
                    }
                    System.out.println("test1SurfaceView.currentDirection-=>"
                            +test1SurfaceView.currentDirection);
                }
                //设置新的观察目标点XZ坐标
                test1SurfaceView.heroXT=test1SurfaceView.heroX+MOVE_XZ[test1SurfaceView.currentDirection][0];
                test1SurfaceView.heroZT=test1SurfaceView.heroZ+MOVE_XZ[test1SurfaceView.currentDirection][1];

                if(keyState==3||keyState==4){
                    Thread.sleep(200);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }
}
