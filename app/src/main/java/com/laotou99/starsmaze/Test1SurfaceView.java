package com.laotou99.starsmaze;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.view.MotionEvent;

import com.alibaba.fastjson.JSON;
import com.laotou99.starsmaze.entity.Ceil;
import com.laotou99.starsmaze.entity.Floor;
import com.laotou99.starsmaze.myThread.KeyThread;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static com.laotou99.starsmaze.constant.Constant.*;

/**
 * Created by wisdom on 2017-12-20.
 */


public class Test1SurfaceView extends GLSurfaceView {

    public MyRender mRender;    //场景渲染器

    public int test1TexId;
    public int ceilTexId;


    public int currentDirection=0; //初始方向为NORTH


    public float heroX=CAMERA_COL * UNIT_SIZE + UNIT_SIZE / 2;	//人眼的XYZ坐标
    public float heroY=0.4f;
    public float heroZ=CAMERA_ROW*UNIT_SIZE+UNIT_SIZE/2;

    public float heroXT=heroX;	//人眼目标点的XYZ坐标
    public float heroYT=0.4f;
    public float heroZT=heroZ-SPEED;

    public Test1SurfaceView(Context context) {
        super(context);
        mRender = new MyRender();
        this.setRenderer(mRender);
        this.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {

        float y = e.getY();
        float x = e.getX();

        float yRatio=y/MainActivity.screenHeight;
        float xRatio=x/MainActivity.screenWidth;

        switch (e.getAction())
        {
            case MotionEvent.ACTION_DOWN://按下动作
//                if(yRatio>0.77f&&yRatio<0.831f&&xRatio>0.756f&&xRatio<0.85f)
//                {//按下前进虚拟按钮
//                    System.out.println("qian");
//                    KeyThread.keyState=1;
//                } else if(yRatio>0.886f&&yRatio<0.948f&&xRatio>0.756f&&xRatio<0.85f) {
//                    //按下后退虚拟按钮
//                    System.out.println("hou");
//                    KeyThread.keyState=2;
//                } else if(yRatio>0.831f&&yRatio<0.886f&&xRatio>0.656f&&xRatio<0.752f) {
//                    //按下左转虚拟按钮
//                    System.out.println("zuo");
//                    KeyThread.keyState=3;
//                } else if(yRatio>0.831f&&yRatio<0.886f&&xRatio>0.85f&&xRatio<0.958f) {
//                    //按下右转虚拟按钮
//                    System.out.println("you");
//                    KeyThread.keyState=4;
//                }else {
//                    KeyThread.keyState=0;
//                }
                if(xRatio>0f&&xRatio<0.5f&&yRatio>0f&&yRatio<0.5f){
                    //按下前进虚拟按钮
                    System.out.println("qian");
                    KeyThread.keyState=1;
                }else if(xRatio>0.5f&&xRatio<1f&&yRatio>0f&&yRatio<0.5f){
                    //按下后退虚拟按钮
                    System.out.println("hou");
                    KeyThread.keyState=2;
                }else if(xRatio>0.5f&&xRatio<1f&&yRatio>0.5f&&yRatio<1f){
                    //按下左转虚拟按钮
                    System.out.println("zuo");
                    KeyThread.keyState=3;
                }else if(xRatio>0f&&xRatio<0.5f&&yRatio>0.5f&&yRatio<1f){
                    //按下右转虚拟按钮
                    System.out.println("you");
                    KeyThread.keyState=4;
                }
                else{
                    KeyThread.keyState=0;
                }



                break;
            case MotionEvent.ACTION_UP://抬起动作
                KeyThread.keyState=0;
                break;
        }


        return true;
    }

    class MyRender implements Renderer{

        Floor floor;//地板
        Ceil ceil;  //天花板

        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            gl.glDisable(GL10.GL_DITHER);

            gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,GL10.GL_FASTEST);

            gl.glClearColor(0,0,0,0);

            gl.glEnable(GL10.GL_DEPTH_TEST);

            test1TexId = initTexture(gl, R.mipmap.abc1);
            ceilTexId = initTexture(gl,R.mipmap.abc2);

            //创建地板
            floor=new Floor(
                    0,                  //x轴初始位置
                    0,                  //z轴初始位置
                    1,                    //是否缩放比例，比例值
                    0,                  //旋转角度值
                    ceilTexId,                //纹理的ID值，为数值整型
                    MAP[0].length,             //width 宽度
                    MAP.length                 //height 高度
            );

            ceil = new Ceil(
                    0,
                    0,
                    1,
                    0,
                    test1TexId,
                    MAP[0].length,
                    MAP.length
            );

            //初始化白色灯
            initWhiteLight(gl);
            //初始化材质
            initMaterial(gl);
        }

        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height)
        {
            //设置视窗大小及位置
            gl.glViewport(0,0,width,height);

            //设置当前矩阵为投影矩阵
            gl.glMatrixMode(GL10.GL_PROJECTION);

            //设置当前矩阵为单位矩阵
            gl.glLoadIdentity();

            //计算透视投影的比例
            float ratio = (float)width/height;

            //调用此方法计算产生透视投影矩阵
            gl.glFrustumf(-ratio*0.6f,ratio*0.6f,-1,1,NEAR,100);
        }

        @Override
        public void onDrawFrame(GL10 gl) {

            //采用平滑着色
            gl.glShadeModel(GL10.GL_SMOOTH);

            //设置为打开背面剪裁
            gl.glEnable(GL10.GL_CULL_FACE);

            //清除颜色缓存于深度缓存
            gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

            //设置当前矩阵为模式矩阵
            gl.glMatrixMode(GL10.GL_MODELVIEW);

            //设置当前矩阵为单位矩阵
            gl.glLoadIdentity();

            //允许光照
            gl.glEnable(GL10.GL_LIGHTING);

            //设定白色光源的位置
            float[] positionParams = {heroX,heroY,heroZ};//采用定位光
            gl.glLightfv(
                    GL10.GL_LIGHTING,
                    GL10.GL_POSITION,
                    positionParams,
                    0
            );

            //设定视点位置方向
            GLU.gluLookAt(
                    gl,
                    heroX,      //人眼位置的X
                    heroY,      //人眼位置的Y
                    heroZ,      //人眼位置的Z
                    heroXT,     //人眼球看的点X
                    heroYT,     //人眼球看的点Y
                    heroZT,     //人眼球看的点Z
                    0,
                    1,
                    0
            );

            //绘制地板
            gl.glPushMatrix();
            gl.glTranslatef(0,FLOOR_Y,0);
            floor.drawSelf(gl);
            gl.glPopMatrix();

            //绘制天花板
            gl.glPushMatrix();
            gl.glTranslatef(0,CELL_Y,0);
            ceil.drawSelf(gl);
            gl.glPopMatrix();

            //恢复成初始状态绘制晶体图标与数量
            //设置当前矩阵为模式矩阵
            gl.glMatrixMode(GL10.GL_MODELVIEW);
            //设置当前矩阵为单位矩阵
            gl.glLoadIdentity();

            //禁止光照
            gl.glDisable(GL10.GL_LIGHTING);
            //开启混合
            gl.glEnable(GL10.GL_BLEND);
            //设置源混合因子与目标混合因子
            gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

            gl.glPopMatrix();
            //禁止混合
            gl.glDisable(GL10.GL_BLEND);
        }
    }

    //初始化纹理
    public int initTexture(GL10 gl,int drawableId)
    {
        //生成纹理ID
        int[] textures = new int[1];
        gl.glGenTextures(1,textures,0);
        System.out.println("textures-=>"+ JSON.toJSONString(textures));
        int currTextureId = textures[0];
        System.out.println("currTextureId-=>"+currTextureId);
        gl.glBindTexture(GL10.GL_TEXTURE_2D,currTextureId);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_MAG_FILTER,GL10.GL_LINEAR);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_WRAP_S,GL10.GL_REPEAT);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_WRAP_T,GL10.GL_REPEAT);

        InputStream is = this.getResources().openRawResource(drawableId);
        Bitmap bitmapTmp;
        try {
            bitmapTmp = BitmapFactory.decodeStream(is);
        }finally {
            try {
                is.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D,0,bitmapTmp,0);
        bitmapTmp.recycle();

        return currTextureId;
    }

    //初始化白色灯
    private void initWhiteLight(GL10 gl)
    {
        gl.glEnable(GL10.GL_LIGHT1);//打开1号灯

        //环境光设置
        float[] ambientParams={0.2f,0.2f,0.05f,1.0f};//光参数 RGBA
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, ambientParams,0);

        //散射光设置
        float[] diffuseParams={0.9f,0.9f,0.2f,1.0f};//光参数 RGBA
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_DIFFUSE, diffuseParams,0);

        //反射光设置
        float[] specularParams={1f,1f,1f,1.0f};//光参数 RGBA
        gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_SPECULAR, specularParams,0);
    }

    //初始化材质
    private void initMaterial(GL10 gl)
    {//材质为白色时什么颜色的光照在上面就将体现出什么颜色
        //环境光为白色材质
        float ambientMaterial[] = {1.0f, 1.0f, 1.0f, 1.0f};
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, ambientMaterial,0);
        //散射光为白色材质
        float diffuseMaterial[] = {1.0f, 1.0f, 1.0f, 1.0f};
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, diffuseMaterial,0);
        //高光材质为白色
        float specularMaterial[] = {1f, 1f, 1f, 1.0f};
        gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, specularMaterial,0);
        gl.glMaterialf(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, 4.0f);
    }
}
