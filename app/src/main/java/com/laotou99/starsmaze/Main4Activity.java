package com.laotou99.starsmaze;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

import static com.laotou99.starsmaze.constant.Constant.*;
import com.laotou99.starsmaze.myThread.KeyThread;

public class Main4Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main4);

        Test1SurfaceView tsv1 = new Test1SurfaceView(this);
        setContentView(tsv1);
        threadWork=true;
        new KeyThread(tsv1).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        threadWork=false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        threadWork=true;
    }
}
