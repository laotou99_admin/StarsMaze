package com.laotou99.starsmaze;

import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main3);

        GLSurfaceView glView = new GLSurfaceView(this);
        RenderTest tr1 = new RenderTest(this);
        glView.setRenderer(tr1);

        setContentView(glView);

    }
}
