package com.laotou99.starsmaze;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by wisdom on 2017-12-20.
 */

public class RenderTest implements GLSurfaceView.Renderer {

    private Context mContext;

    //三角形
    float[] triangleData = new float[]{
            0.1f,0.6f,0.0f,
            -0.3f,0.0f,0.0f,
            0.3f,0.1f,0.1f
    };

    int[] triangleColor = new int[]{
            65535,0,0,0,
            0,65535,0,0,
            0,0,65535,0
    };


    //三菱锥
    float[] taperVertices = new float[]{
            0.0f,   0.5f,   0.0f,
            -0.5f,  -0.5f,  -0.2f,
            0.5f,   -0.5f,  -0.2f,
            0.0f,   -0.2f,  0.2f
    };
    int[] taperColors = new int[]{
            65535,  0,  0,  0,
            0,  65535,  0,  0,
            0,  0,  65535,  0,
            65535,  65535,  0,  0
    };
    byte[] taperFacets = new byte[]{
            0,1,2,
            0,1,3,
            1,2,3,
            0,2,3
    };


    //长方形
    float[] rectData = new float[]{
            0.5f,   0,5f,   0.0f,
            0.5f,   -0.5f,  0.0f,
            -0.5f,  0.5f,   0.0f,
            -0.5f,  -0.5f,  0.0f
    };

    float[] rectTex = new float[]{
            0f,     0f,
            2.0f,   0f,
            0f,   2.0f,
            2.0f,     2.0f
    };

    int[] rectColor = new int[]{
            0,  65535,  0,   0,
            0,  0,  65535,  0,
            65535,  0,  0,  0,
            65535,  65535,  0,  0
    };

    int texture = 0;

    //正方形
    FloatBuffer rectDataBuffer;
    IntBuffer rectColorBuffer;
    FloatBuffer rectTexBuffer;

    //三菱锥
    FloatBuffer taperVerticesBuffer;
    IntBuffer taperColorsBuffer;
    ByteBuffer taperFacetsBuffer;

    //三角形
    FloatBuffer triangleDataBuffer;
    IntBuffer triangleColorBuffer;

    private float rotate;

    public RenderTest(Context mContext){
        this.mContext = mContext;
        triangleDataBuffer = floatBufferUtil(triangleData);
        triangleColorBuffer = intBufferUtil(triangleColor);
//        rotate = 1.1f;
        taperVerticesBuffer = floatBufferUtil(taperVertices);
        taperColorsBuffer = intBufferUtil(taperColors);
        taperFacetsBuffer = ByteBuffer.wrap(taperFacets);

        rectDataBuffer = floatBufferUtil(rectData);
        rectColorBuffer = intBufferUtil(rectColor);
        rectTexBuffer = floatBufferUtil(rectTex);
    }

    private IntBuffer intBufferUtil(int[] arr) {
        IntBuffer mBuffer = null;
        ByteBuffer qbb = ByteBuffer.allocateDirect(arr.length*4);
        qbb.order(ByteOrder.nativeOrder());
        mBuffer = qbb.asIntBuffer();
        mBuffer.put(arr);
        mBuffer.position(0);
        return mBuffer;
    }

    private FloatBuffer floatBufferUtil(float[] arr) {
        FloatBuffer mBuffer = null;
        ByteBuffer qbb = ByteBuffer.allocateDirect(arr.length*4);
        qbb.order(ByteOrder.nativeOrder());
        mBuffer = qbb.asFloatBuffer();
        mBuffer.put(arr);
        mBuffer.position(0);
        return mBuffer;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glDisable(GL10.GL_DITHER);

        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,GL10.GL_FASTEST);

        gl.glClearColor(0,0,0,0);

        gl.glShadeModel(GL10.GL_SMOOTH);

        gl.glEnable(GL10.GL_DEPTH_TEST);

        gl.glDepthFunc(GL10.GL_LEQUAL);

        gl.glEnable(GL10.GL_TEXTURE_2D);
        loadTexture(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

        gl.glViewport(0,0,width,height);

        gl.glMatrixMode(GL10.GL_PROJECTION);

        gl.glLoadIdentity();

        float ratio =  (float)width/height;

        gl.glFrustumf(-ratio,ratio,-1,1,1,10);

    }

    @Override
    public void onDrawFrame(GL10 gl) {

        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

        gl.glEnableClientState(GL10.GL_COLOR_ARRAY);

        gl.glMatrixMode(GL10.GL_MODELVIEW);

        //---FIRST DRAW-----
        gl.glLoadIdentity();

        gl.glTranslatef(-0.32f,0.35f,-1.2f);
        gl.glRotatef(rotate,0f,0.5f,0f);//旋转

        gl.glVertexPointer(3,GL10.GL_FLOAT,0,triangleDataBuffer);

        gl.glColorPointer(4,GL10.GL_FIXED,0,triangleColorBuffer);

        gl.glDrawArrays(GL10.GL_TRIANGLES,0,3);

        //--------第二个 图形-------------------------------------------------------------
        gl.glLoadIdentity();
        gl.glTranslatef(0.7f,0.0f,-2.2f);
        gl.glRotatef(rotate,0f,0.2f,0f);

        gl.glVertexPointer(3,GL10.GL_FLOAT,0,taperVerticesBuffer);
        gl.glColorPointer(4,GL10.GL_FIXED,0,taperColorsBuffer);

        gl.glDrawElements(GL10.GL_TRIANGLE_STRIP,taperFacetsBuffer.remaining(),
                GL10.GL_UNSIGNED_BYTE,taperFacetsBuffer);

        gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        //-------第三个 图形-----------------------------------------------------------------
        gl.glLoadIdentity();
        gl.glTranslatef(-0.1f,-0.5f,-3.8f);
        gl.glRotatef(rotate,0f,0f,0.5f);

        gl.glVertexPointer(3,GL10.GL_FLOAT,0,rectDataBuffer);
//        gl.glColorPointer(4,GL10.GL_FIXED,0,rectColorBuffer);
        gl.glTexCoordPointer(2,GL10.GL_FLOAT,0,rectTexBuffer);
        gl.glBindTexture(GL10.GL_TEXTURE_2D,texture);
//        gl.glDrawElements(GL10.GL_TRIANGLE_STRIP);
        gl.glDrawArrays(GL10.GL_TRIANGLES,0,4);

        gl.glFinish();
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
//        gl.glDisableClientState(GL10.GL_COLOR_ARRAY);

        rotate += 1;
        if(rotate>360){
            rotate = 0;
        }
    }

    private void loadTexture(GL10 gl){
        Bitmap bitmap = null;
        try{
            bitmap = BitmapFactory.decodeResource(
                    mContext.getResources(),R.mipmap.eee1);
            int[] textures = new int[1];
            gl.glGenTextures(1,textures,0);
            texture =textures[0];
            System.out.println("texture-=>"+texture);
            gl.glBindTexture(GL10.GL_TEXTURE_2D,texture);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D,
                    GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_NEAREST);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D,
                    GL10.GL_TEXTURE_MAG_FILTER,GL10.GL_LINEAR);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D,
                    GL10.GL_TEXTURE_WRAP_S,GL10.GL_REPEAT);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D,
                    GL10.GL_TEXTURE_WRAP_T,GL10.GL_REPEAT);
            GLUtils.texImage2D(GL10.GL_TEXTURE_2D,0,bitmap,0);
        }finally {
            if(bitmap!=null){
                bitmap.recycle();
            }
        }
    }
}
