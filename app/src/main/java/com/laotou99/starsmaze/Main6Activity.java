package com.laotou99.starsmaze;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

public class Main6Activity extends AppCompatActivity {

    int[] iMaze;

    ImageButton btn11,btn12,btn13,btn14,btn15,btn16,
            btn21,btn22,btn23,btn24,btn25,btn26,
            btn31,btn32,btn33,btn34,btn35,btn36,
            btn41,btn42,btn43,btn44,btn45,btn46,
            btn51,btn52,btn53,btn54,btn55,btn56,
            btn61,btn62,btn63,btn64,btn65,btn66;

    List<ImageButton> btnList1 = new ArrayList<ImageButton>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);

        initButtons();
        buttonEvents();
        addToList();
    }

    public void showMaze(){

    }

    private void addToList() {
        btnList1.add(btn11);
        btnList1.add(btn12);
        btnList1.add(btn13);
        btnList1.add(btn14);
        btnList1.add(btn15);
        btnList1.add(btn16);
        btnList1.add(btn21);
        btnList1.add(btn22);
        btnList1.add(btn23);
        btnList1.add(btn24);
        btnList1.add(btn25);
        btnList1.add(btn26);
        btnList1.add(btn31);
        btnList1.add(btn32);
        btnList1.add(btn33);
        btnList1.add(btn34);
        btnList1.add(btn35);
        btnList1.add(btn36);
        btnList1.add(btn41);
        btnList1.add(btn42);
        btnList1.add(btn43);
        btnList1.add(btn44);
        btnList1.add(btn45);
        btnList1.add(btn46);
        btnList1.add(btn51);
        btnList1.add(btn52);
        btnList1.add(btn53);
        btnList1.add(btn54);
        btnList1.add(btn55);
        btnList1.add(btn56);
        btnList1.add(btn61);
        btnList1.add(btn62);
        btnList1.add(btn63);
        btnList1.add(btn64);
        btnList1.add(btn65);
        btnList1.add(btn66);
    }

    private void buttonEvents() {

        btn11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn35.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn36.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn43.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn44.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn45.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn46.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn52.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn53.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn54.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn55.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn56.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn61.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn62.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn63.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn64.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn65.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn66.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    private void initButtons() {
        btn11 = (ImageButton)findViewById(R.id.imageButton11);
        btn12 = (ImageButton)findViewById(R.id.imageButton12);
        btn13 = (ImageButton)findViewById(R.id.imageButton13);
        btn14 = (ImageButton)findViewById(R.id.imageButton14);
        btn15 = (ImageButton)findViewById(R.id.imageButton15);
        btn16 = (ImageButton)findViewById(R.id.imageButton16);


        btn21 = (ImageButton)findViewById(R.id.imageButton21);
        btn22 = (ImageButton)findViewById(R.id.imageButton22);
        btn23 = (ImageButton)findViewById(R.id.imageButton23);
        btn24 = (ImageButton)findViewById(R.id.imageButton24);
        btn25 = (ImageButton)findViewById(R.id.imageButton25);
        btn26 = (ImageButton)findViewById(R.id.imageButton26);


        btn31 = (ImageButton)findViewById(R.id.imageButton31);
        btn32 = (ImageButton)findViewById(R.id.imageButton32);
        btn33 = (ImageButton)findViewById(R.id.imageButton33);
        btn34 = (ImageButton)findViewById(R.id.imageButton34);
        btn35 = (ImageButton)findViewById(R.id.imageButton35);
        btn36 = (ImageButton)findViewById(R.id.imageButton36);


        btn41 = (ImageButton)findViewById(R.id.imageButton41);
        btn42 = (ImageButton)findViewById(R.id.imageButton42);
        btn43 = (ImageButton)findViewById(R.id.imageButton43);
        btn44 = (ImageButton)findViewById(R.id.imageButton44);
        btn45 = (ImageButton)findViewById(R.id.imageButton45);
        btn46 = (ImageButton)findViewById(R.id.imageButton46);


        btn51 = (ImageButton)findViewById(R.id.imageButton51);
        btn52 = (ImageButton)findViewById(R.id.imageButton52);
        btn53 = (ImageButton)findViewById(R.id.imageButton53);
        btn54 = (ImageButton)findViewById(R.id.imageButton54);
        btn55 = (ImageButton)findViewById(R.id.imageButton55);
        btn56 = (ImageButton)findViewById(R.id.imageButton56);


        btn61 = (ImageButton)findViewById(R.id.imageButton61);
        btn62 = (ImageButton)findViewById(R.id.imageButton62);
        btn63 = (ImageButton)findViewById(R.id.imageButton63);
        btn64 = (ImageButton)findViewById(R.id.imageButton64);
        btn65 = (ImageButton)findViewById(R.id.imageButton65);
        btn66 = (ImageButton)findViewById(R.id.imageButton66);

    }
}
